var roleSpawner = {
    run: function() {
        for(var name in Memory.creeps) {
            if(!Game.creeps[name]) {
                delete Memory.creeps[name];
                console.log('Clearing non-existing creep memory:', name);
            }
        }
        
        this.spawnCreep('upgrader', 2);
        this.spawnCreep('builder', 2);
        this.spawnCreep('harvester', 2);
    },
    spawnCreep: function(role, number) {
        var foundCreeps = _.filter(Game.creeps, (creep) => creep.memory.role == role);
        console.log(role + ': ' + foundCreeps.length);

        
        if (foundCreeps.length < number) {
            var creepName = role + Game.time;
            console.log('Spawning new creep: ' + creepName);
            Game.spawns['Spawn1'].spawnCreep([WORK, CARRY, MOVE], creepName, { memory: { role: role }});
        }
    }
};

module.exports = roleSpawner;